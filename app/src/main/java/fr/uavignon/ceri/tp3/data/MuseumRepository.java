package fr.uavignon.ceri.tp3.data;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import fr.uavignon.ceri.tp3.data.database.MuseumDao;
import fr.uavignon.ceri.tp3.data.database.MuseumRoomDatabase;
import fr.uavignon.ceri.tp3.data.webservice.ItemResponse;
import fr.uavignon.ceri.tp3.data.webservice.ItemResult;
import fr.uavignon.ceri.tp3.data.webservice.OWMInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;

import static fr.uavignon.ceri.tp3.data.database.MuseumRoomDatabase.databaseWriteExecutor;

public class MuseumRepository {

    private static final String TAG = MuseumRepository.class.getSimpleName();

    private LiveData<List<Item>> collection;
    private MutableLiveData<Item> selectedItem;

    private MuseumDao museumDao;


    private static volatile MuseumRepository INSTANCE;
    private final OWMInterface api;

    public synchronized static MuseumRepository get(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new MuseumRepository(application);
        }

        return INSTANCE;
    }

    public MuseumRepository(Application application) {
        MuseumRoomDatabase db = MuseumRoomDatabase.getDatabase(application);
        museumDao = db.MuseumDao();
        collection = museumDao.getAllCities();
        selectedItem = new MutableLiveData<>();

        Retrofit retrofit =
                new Retrofit.Builder().baseUrl("https://demo-lia.univ-avignon.fr/cerimuseum/")
                        .addConverterFactory(MoshiConverterFactory.create())
                        .build();
        api = retrofit.create(OWMInterface.class);
    }

    public LiveData<List<Item>> getAllCollection() {
        return collection;
    }

    public MutableLiveData<Item> getSelectedItem() {
        return selectedItem;
    }

    public void loadItem(Item item){

        final MutableLiveData<ItemResult> result = new MutableLiveData<>();

        result.setValue(new ItemResult(true, null,null));
        api.getAllCollection().enqueue(
                new Callback<ItemResponse>() {
                    @Override
                    public void onResponse(Call<ItemResponse> call, Response<ItemResponse> response) {
                        result.postValue(new ItemResult(false, response.body().properties.item, null)) ;
                        //  Log.e("weather","exception loading");
                    }

                    @Override
                    public void onFailure(Call<ItemResponse> call, Throwable t) {

                        result.postValue(new ItemResult(false,null,t));

                    }
                }
        );

    }




    public long insertItem(Item newItem) {
        Future<Long> flong = databaseWriteExecutor.submit(() -> {
            return museumDao.insert(newItem);
        });
        long res = -1;
        try {
            res = flong.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedItem.setValue(newItem);
        return res;
    }

    public int updateItem(Item item) {
        Future<Integer> fint = databaseWriteExecutor.submit(() -> {
            return museumDao.update(item);
        });
        int res = -1;
        try {
            res = fint.get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (res != -1)
            selectedItem.setValue(item);
        return res;
    }

    public void deleteItem(long id) {
        databaseWriteExecutor.execute(() -> {
            museumDao.deleteItem(id);
        });
    }

    public void getItem(long id)  {
        Future<Item> fitem = databaseWriteExecutor.submit(() -> {
            Log.d(TAG,"selected id="+id);
            return museumDao.getItemById(id);
        });
        try {
            selectedItem.setValue(fitem.get());
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
