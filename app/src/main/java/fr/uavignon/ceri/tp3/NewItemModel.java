package fr.uavignon.ceri.tp3;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.tp3.data.Item;
import fr.uavignon.ceri.tp3.data.MuseumRepository;

public class NewItemModel extends AndroidViewModel {
    public static final String TAG = DetailViewModel.class.getSimpleName();

    private MuseumRepository repository;
    private MutableLiveData<Item> item;

    public NewItemModel(Application application) {
        super(application);
        repository = MuseumRepository.get(application);
        item = new MutableLiveData<>();
    }

    public void setItem(long id) {
        repository.getItem(id);
        item = repository.getSelectedItem();
    }

    LiveData<Item> getCity() {
        return item;
    }

    public long insertOrUpdateItem(Item newItem) {
        long res = 0;
        if (item.getValue() == null) {
            res = repository.insertItem(newItem);
            // return -1 if there is a conflict
            setItem(res);
        } else {
            // ID does not change for updates
            newItem.setId(item.getValue().getId());
            int nb = repository.updateItem(newItem);
            // return the nb of rows updated by the query
            if (nb ==0)
                res = -1;
        }
        Log.d(TAG,"insert item="+item.getValue());
        return res;
    }
}
