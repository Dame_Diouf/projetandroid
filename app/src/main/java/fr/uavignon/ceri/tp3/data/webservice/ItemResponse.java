package fr.uavignon.ceri.tp3.data.webservice;

import java.util.List;

public class ItemResponse {

    public final Properties properties = null;

    public static class Properties {
        public final List<Forecast> item = null;
    }

    public class Forecast {

        public String name;
        public String brand;
        public String description;
        public List<String> categories;

        public Forecast(String name, String brand, String description, List<String> categories){

            this.name = name;
            this.brand = brand;
            this.description = description;
            this.categories = categories;
        }
    }
}
