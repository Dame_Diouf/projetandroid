package fr.uavignon.ceri.tp3.data.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import fr.uavignon.ceri.tp3.data.Item;

@Dao
public interface MuseumDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Item item);

    @Query("DELETE FROM museum_table")
    void deleteAll();

    @Query("SELECT * from museum_table ORDER BY name ASC")
    LiveData<List<Item>> getAllCities();

    @Query("SELECT * from museum_table ORDER BY name ASC")
    List<Item> getSynchrAllCities();

    @Query("DELETE FROM museum_table WHERE _id = :id")
    void deleteItem(double id);

    @Query("SELECT * FROM museum_table WHERE _id = :id")
    Item getItemById(double id);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    int update(Item item);
}
