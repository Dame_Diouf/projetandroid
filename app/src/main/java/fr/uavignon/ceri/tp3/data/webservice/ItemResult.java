package fr.uavignon.ceri.tp3.data.webservice;

import java.util.List;

import fr.uavignon.ceri.tp3.data.Item;

public class ItemResult {



        public final boolean isLoading;
        public final List<ItemResponse.Forecast> forecasts;
        public final Throwable error;

        public ItemResult(boolean isLoading, List<ItemResponse.Forecast> forecasts, Throwable error){

            this.isLoading = isLoading;
            this.forecasts = forecasts;
            this.error = error;
        }

        void transferInfo(ItemResponse weatherInfo, Item ItemInfo){

        }
}
