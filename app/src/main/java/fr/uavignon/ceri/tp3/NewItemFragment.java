package fr.uavignon.ceri.tp3;

import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.google.android.material.snackbar.Snackbar;

import fr.uavignon.ceri.tp3.data.Item;

public class NewItemFragment extends Fragment {


    public static final String TAG = NewItemFragment.class.getSimpleName();

    private NewItemModel viewModel;
    private EditText editNewName, editNewBrand, editNewCategorie;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(NewItemModel.class);

        // Get selected city
        NewItemFragmentArgs args = NewItemFragmentArgs.fromBundle(getArguments());
        long itemID = args.getCityNum();
        Log.d(TAG, "selected id=" + itemID);
        viewModel.setItem(itemID);

        listenerSetup();
        observerSetup();

    }


    private void listenerSetup() {
        editNewName = getView().findViewById(R.id.editNewName);
        editNewBrand = getView().findViewById(R.id.editNewCountry);

        getView().findViewById(R.id.buttonInsert).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editNewName.getText().toString().isEmpty() || editNewBrand.getText().toString().isEmpty())
                    Snackbar.make(view, "",
                            Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                else
                    if (viewModel.insertOrUpdateItem(new Item(editNewName.getText().toString(), editNewBrand.getText().toString())) == -1) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage("")
                                .setTitle("Erreur à l'ajout");
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    } else {
                        Snackbar.make(view, "",
                                Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
            }
        });

      /*  getView().findViewById(R.id.buttonBack2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp3.NewCityFragment.this)
                        .navigate(R.id.action_NewCityFragment_to_ListFragment);
            }
        });*/
    }

    private void observerSetup() {
        viewModel.getCity().observe(getViewLifecycleOwner(),
                city -> {
                    if (city != null) {
                        Log.d(TAG, "observing city view");
                        editNewName.setText(city.getName());
                        editNewName.setText(city.getBrand());
                        //editNewBrand.setText(city.getCategories());
                    }
                });

    }
}
