package fr.uavignon.ceri.tp3;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import fr.uavignon.ceri.tp3.data.Item;
import fr.uavignon.ceri.tp3.data.MuseumRepository;

public class DetailViewModel extends AndroidViewModel {
    public static final String TAG = DetailViewModel.class.getSimpleName();

    private MuseumRepository repository;
    private MutableLiveData<Item> item;

    public DetailViewModel (Application application) {
        super(application);
        repository = MuseumRepository.get(application);
        item = new MutableLiveData<>();
    }

    public void setItem(long id) {
        repository.getItem(id);
        item = repository.getSelectedItem();
    }
    LiveData<Item> getItem() {
        return item;
    }
}

