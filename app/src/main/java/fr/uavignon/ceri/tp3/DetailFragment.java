package fr.uavignon.ceri.tp3;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.snackbar.Snackbar;

public class DetailFragment extends Fragment {
    public static final String TAG = DetailFragment.class.getSimpleName();

    private DetailViewModel viewModel;
    private TextView textItemName, textBrand, textCategorie;
    private ImageView imgItem;
    private ProgressBar progress;


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = new ViewModelProvider(this).get(DetailViewModel.class);


        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        long itemID = args.getCityNum();
        Log.d(TAG,"selected id="+itemID);
        viewModel.setItem(itemID);

        listenerSetup();
        observerSetup();

    }


    private void listenerSetup() {
        textItemName = getView().findViewById(R.id.nameItem);
        textBrand = getView().findViewById(R.id.brand);
        textCategorie = getView().findViewById(R.id.categorie);

        imgItem = getView().findViewById(R.id.iconeItem);

        progress = getView().findViewById(R.id.progress);

   /*     getView().findViewById(R.id.buttonUpdate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Interrogation à faire du service web",
                            Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
            }
        });

        getView().findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp3.DetailFragment.this)
                        .navigate(R.id.action_DetailFragment_to_ListFragment);
            }
        });*/
    }

        private void observerSetup () {
            viewModel.getItem().observe(getViewLifecycleOwner(),
                    item -> {
                        if (item != null) {
                            Log.d(TAG, "observing city view");

                            textItemName.setText(item.getName());
                            textBrand.setText(item.getBrand());
                            // textCategorie.setText(item.getCategories());


                            // set ImgView
                            if (item.getImageUri() != null)
                                imgItem.setImageDrawable(getResources().getDrawable(getResources().getIdentifier(item.getImageUri(),
                                        null, getContext().getPackageName())));

                        }
                    });


        }


    }