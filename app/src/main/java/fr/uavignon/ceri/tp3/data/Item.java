package fr.uavignon.ceri.tp3.data;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Entity(tableName = "museum_table"/*, indices = {@Index(value = {"name", "country"},
        unique = true)})*/)
public class Item {

    //public static final String TAG = City.class.getSimpleName();

   // public static final long ADD_ID = -1;

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name="_id")
    private long id;

    @NonNull
    @ColumnInfo(name="name")
    private String name;

    @NonNull
    @ColumnInfo(name="description")
    private String description;

    @NonNull
    @ColumnInfo(name="brand")
    private String brand;

    @ColumnInfo(name="categories")
    private String categories[];


    @ColumnInfo(name="technicalDetails")
    private List<String> technicalDetails;

    @ColumnInfo(name="timeFrame")
    private int timeFrame[];

    @ColumnInfo(name="working")
    private Boolean working;

    @ColumnInfo(name="pictures")
    private HashMap<String, String > pictures;

    @NonNull
    @ColumnInfo(name="brand")
    private String image;



    //@ColumnInfo(name="icon")
    // private String icon; // icon name (ex: 09d)


    public Item(@NonNull String name, String description, String[] categories, List<String> technicalDetails, int timeFrame[], Boolean working) {
        this.name = name;
        this.description = description;
        this.categories = categories;
        this.technicalDetails = technicalDetails;
        this.timeFrame = timeFrame;
        this.working = working;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getBrand() {
        return brand;
    }

    public String[] getCategories() {
        return categories;
    }

    public List<String> getTechnicalDetails() {
        return technicalDetails;
    }

    public int[] getTimeFrame() {
        return timeFrame;
    }

    public String getImage() {
        return image;
    }
    public String getImageUri() {
        if (image == null || image.isEmpty())
            return null;
        else
            return "@drawable/owm_"+image+"_2x";
    }
    public String getSmallImageUri() {
        if (image == null || image.isEmpty())
            return null;
        else
            return "@drawable/owm_"+image;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;

    }

    public void setIcon(HashMap<String,String> pictures) {
        this.pictures = pictures;
    }


    private static Map<String, String> mapCountryNameToCode = getCountryNameToCodeMap();

    @NonNull
    static Map<String, String> getCountryNameToCodeMap() {
        final Map<String, String> displayNameToCountryCode = new HashMap<>();
        for (String countryCode : Locale.getISOCountries()) {
            final Locale locale = new Locale("", countryCode);
            displayNameToCountryCode.put(locale.getDisplayCountry(), countryCode);
        }
        return displayNameToCountryCode;
    }


}
