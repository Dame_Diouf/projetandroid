package fr.uavignon.ceri.tp3;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import fr.uavignon.ceri.tp3.data.Item;
import fr.uavignon.ceri.tp3.data.MuseumRepository;

public class ListViewModel extends AndroidViewModel {
    private MuseumRepository repository;
    private LiveData<List<Item>> allCollection;

    public ListViewModel (Application application) {
        super(application);
        repository = MuseumRepository.get(application);
        allCollection = repository.getAllCollection();
    }

    LiveData<List<Item>> getAllCities() {
        return allCollection;
    }

    public void deleteCity(long id) {
        repository.deleteItem(id);
    }


}
