package fr.uavignon.ceri.tp3.data.database;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import fr.uavignon.ceri.tp3.data.Item;

@Database(entities = {Item.class}, version = 1, exportSchema = false)
public abstract class   MuseumRoomDatabase extends RoomDatabase {

    private static final String TAG = MuseumRoomDatabase.class.getSimpleName();

    public abstract MuseumDao MuseumDao();

    private static MuseumRoomDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 1;
    public static final ExecutorService databaseWriteExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);


    public static MuseumRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (MuseumRoomDatabase.class) {
                if (INSTANCE == null) {
                    // Create database here
                            // without populate
                        /*
                    INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    WeatherRoomDatabase.class,"book_database")
                                    .build();

                     */

                            // with populate
                            INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(),
                                    MuseumRoomDatabase.class,"book_database")
                                    .addCallback(sRoomDatabaseCallback)
                                    .build();

                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);

                    databaseWriteExecutor.execute(() -> {
                        // Populate the database in the background.
                        MuseumDao dao = INSTANCE.MuseumDao();
                        dao.deleteAll();

                      /*
                      Item[] cities = {new Item("Avignon", "France"),
                        new Item("Paris", "France"),
                        new Item("Rennes", "France"),
                        new Item("Montreal", "Canada"),
                        new Item("Rio de Janeiro", "Brazil"),
                        new Item("Papeete", "French Polynesia"),
                        new Item("Sydney", "Australia"),
                        new Item("Seoul", "South Korea"),
                        new Item("Bamako", "Mali"),
                        new Item("Istanbul", "Turkey")};


                         for(Item newItem : cities)
                            dao.insert(newItem);
                        Log.d(TAG,"database populated");

                       */
                    });

                }
            };
}
