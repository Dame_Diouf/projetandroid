package fr.uavignon.ceri.tp3.data.webservice;

//import fr.uavignon.ceri.tp3.data.City;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface OWMInterface {
  //  @Headers("Accept: application/geo+json")
   // @GET("/collection")
     @GET("/collection")
     Call<ItemResponse> getAllCollection();

}